# README #

The main goal of this public repository is to provide the (software) resources that complement the paper "A tool supported methodology to passively test asynchronous systems with multiple users". 
Authors: Mercedes G. Merayo (mgmerayo@fdi.ucm.es), Robert M. Hierons (rob.hierons@brunel.ac.uk) and Manuel Núñez (mn@sip.ucm.es).

### What is this repository for? ###

The main aim of this repository is to provide the reader with the needed resources to rerun the experiments that are shown in this paper. There are 4 directories containing the following files:

* Automata: Automata generated for each property of the experiments. They are SVG files that can be edited or viewed with any vector graphics editor (e.g. Inkscape).
* Properties: XML files for each of the properties that are used in the experiments for each protocol.
* Tools: Three sub-directories for the tools PTTAC (EXE file for Windows 64bits systems and JAR file for Ubuntu 64bits systems), MergeLogs and TraceTranslator. Some technical aspects are required, so it is recommended to follow the advices of the next sections.
* Traces: Example of a XML file that represents a captured trace. 

### How do I set up the systems? ###

In order to use the EXE and JAR files of PTTAC it is necessary to check these requirements:

* OS: Windows 7/8/10, Ubuntu 12.04 LTS and up. 
* Java version: 1.8.0_45v and up.
* Java SE Runtime Environment (JRE): 8u77v and up.
* GraphViz Software: 2.38v and up.
* WinPcap: 4.1.3 and up.
* WinDump: 3.9.5 and up.

In order to use the Java Project of the paper it is also necessary to check these requirements:

* Java Development Kit (JDK): 8u77v and up.
* Apache Maven: 3.3.9v and up.


### MergeLogs Instructions ###

This software allows us to join logs from different users and obtain an XML file that PTTAC needs to test the complete trace against the properties. Its execution shows the steps that must be follow to complete a correct translation of files.


### TraceTranslator Instructions ###

This software allows users to translate from the plain text files including traces captured with Wireshark to  XML files that PTTAC needs to test them against the properties. Its execution shows the steps that must be follow to complete a correct translation of files.

### PTTAC Instructions ###

This software is the key of this study and it checks the correctness of some traces against previously defined properties. If the previous technical requirements are fulfilled, then the execution of a property and the checking of a trace is done with these steps:

* Go to your home directory and create the following directory: \home\PTTAC\Properties\MultipleUsers.
* Download the four properties of the directory "Properties" of this repository and put them in the directory "MultipleUsers" that has been created in the previous step.
* Open the PTTAC application using the EXE or JAR file.
* In the tree that is in the left of the screen, there should be five properties. Double click on the "Listing" property and its characteristics will be shown on the right side of the screen.
* Click on the "Graph" button of the button group "Graphic" and it generates the graph and the automaton of the property.
* Now we have two options: capture an online communication between two systems or test one of the traces that are in the "traces" directory of this repository. In case of: 

-Online traces, it is necessary admin privileges because it is necessary to access the network card. Click the "Capture" tab of PTTAC and write the name of the capture, the protocol that you want to check (HTTP, FTP or TCP) and select the interface where you are going to listen the communication. Then, click on the "Capture" button of the "Test" buttons group and check the results that PTTAC shows.

-Offline traces, download the traces of this repository and import them. Click on the "Import" button of the "Test" buttons group and select the path where you downloaded it. Then, click on the "Test" tab of PTTAC and select the imported trace from the list that you can open in the right side of the screen. After select the trace, click on the "Start test" button of the "Test" buttons group and check the results that PTTAC shows.

* At the end of the test, there is shown the evolution of the test through the different actions that conform the trace.